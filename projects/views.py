from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def list_projects(request):
    home = Project.objects.filter(owner=request.user)
    context = {
        "home": home,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    proj_list = get_object_or_404(Project, id=id)
    context = {
        "proj_list": proj_list,
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "project_form": form,
    }
    return render(request, "projects/create_project.html", context)
