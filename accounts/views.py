from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from accounts.forms import AccountLogin, SignUpForm


# Create your views here.
def account_login(request):
    if request.method == "POST":
        form = AccountLogin(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = AccountLogin()
    context = {
        "login_form": form,
    }
    return render(request, "accounts/login.html", context)


def account_logout(request):
    logout(request)
    return redirect("login")


def account_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password,
                )

                login(request, user)
                return redirect("list_projects")
    else:
        form = SignUpForm()
    context = {
        "signupform": form,
    }
    return render(request, "accounts/signup.html", context)
